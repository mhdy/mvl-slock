# slock - simple screen locker

simple screen locker utility for X.

## Requirements

In order to build slock you need the Xlib header files.

## Installation

Edit config.mk to match your local setup (slock is installed into
the `~/.local` namespace by default).

Afterwards enter the following command to build and install slock: `make clean install`.

The slock binary requires to be owned by root and the sticky bit should be set.

```
sudo chown root:root ~/.local/bin/slock
sudo chmod u+s ~/.local/bin/slock
```

## Running slock

Simply invoke the 'slock' command. To get out of it, enter your password.
